<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Company\Printer;

/**
 * Description of ListPrinter
 *
 * @author test
 */
class ListPrinter {

    public function show($models) {
        $strings = [];
        foreach ($models as $model) {
           $strings[] = print_r($model, true);
        }
        return implode("/n",$strings);
    }

}
