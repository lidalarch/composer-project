<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Company\Model;

use DateTime;

/**
 * Description of Currency
 *
 * @author test
 */
abstract class Currency implements Model {

    protected $cost;
    protected $dateAt;
    protected $code;
    
    public function getCode():string {
        return $this->code;
    }
    
    public function setCode(string $code) {
        $this->code = $code;
    }
    
    public function getCost(): float {
        return $this->cost;
    }

    public function setCost(float $cost) {
        $this->cost = $cost;
    }

    public function getDateAt(): DateTime {
        return $this->dateAt;
    }

    public function setDateAt(DateTime $dateAt) {
        $this->dateAt = $dateAt;
    }

}
