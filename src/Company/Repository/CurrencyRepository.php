<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Company\Repository;

/**
 * Description of CurrencyRepository
 *
 * @author test
 */
use Company\DataSource\Source;
use Company\Model\Model;
use DateTime;
use Company\Model\Currency;
use Company\Model\Rur;
use Company\Model\Usd;

class CurrencyRepository implements Repository {
/**
     * @var Source
     */
    protected $source;

    public function __construct(Source $source) {
        $this->source = $source;
    }

    public function list(): array {
        $rawData = $this->source->getAll();
        $currencies = [];

        foreach ($rawData as $currencyRawData) {
            $currencies[] = $this->createInstance($currencyRawData);
        }
        return $currencies;
    }

    public function get(DateTime $date) {
        $rawData = $this->source->getOneByDate($date);
        if(!$rawData) {
            return null;
        }
        return $this->createInstance($rawData);
    }

    public function save(Model $currency): bool {
        /*         * @var Currency $currency */
        return $this->source->insert([
                    'code' => $currency->getCode(),
                    'cost' => $currency->getCost(),
                    'date_at' => $currency->getDateAt()->format('Y-m-d'),
        ]);
    }

    private function createInstance(array $rawData): Model {
        if ($rawData['code'] === 'USD') {
            $currency = new Usd();
        } else {
            $currency = new Rur();
        }
        $currency->setCost($rawData['cost']);
        date_default_timezone_set('ASIA/Novosibirsk');
        $dateAt = DateTime::createFromFormat('Y-m-d H:i:s', $rawData['date_at']);
        $currency->setDateAt($dateAt);
        
        return $currency;
    }

}
