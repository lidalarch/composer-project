<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Company\Repository;

use DateTime;
use Company\Model\Model;
use Company\DataSource\Source;
/**
 * Description of Repository
 *
 * @author test
 */


interface Repository {

    public function __construct(Source $source);

    public function list(): array;

    public function get(DateTime $date);

    public function save(Model $model): bool;
}
