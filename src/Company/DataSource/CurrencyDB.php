<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Company\DataSource;

use DateTime;
use PDO;

/**
 * Класс для работы с базой данных валют
 *
 * @package DTelepnev\DataSource
 * @see https://www.cbr.ru/development/
 * @todo выборку по стоимостям валют
 */
class CurrencyDB implements Source {

    /**
     * @var PDO
     */
    protected $dbConn;

    /**
     * CurrencyDB constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo) {
        $this->dbConn = $pdo;
    }

    /**
     * @inheritDoc
     * @param array $currency
     * @return bool
     */
    public function insert(array $currency): bool {
        $insertSql = 'insert into currencies (code, cost, date_at) values (:code, :cost, :dateAt)';
        $stmt = $this->dbConn->prepare($insertSql);
        return $stmt->execute([
                    ':code' => $currency['code'],
                    ':cost' => $currency['cost'],
                    ':dateAt' => $currency['date_at']
        ]);
    }

    /**
     * @inheritDoc
     * @param DateTime $date
     * @see \CurrencyDBTest
     * @throws
     * @return array
     */
    public function getOneByDate(DateTime $date): array {
        $findByDate = 'SELECT * FROM currencies WHERE date_at = :dateAt LIMIT 1';
        $stmt = $this->dbConn->prepare($findByDate);
        $stmt->execute([':dateAt' => $date->format('Y-m-d')]);
        return $stmt->fetch();
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function getAll(): array {
        $getAll = 'SELECT * FROM currencies';
        $stmt = $this->dbConn->prepare($getAll);
        $stmt->execute();

        return $stmt->fetchAll();
    }

}
