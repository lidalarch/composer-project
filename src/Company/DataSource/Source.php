<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Company\DataSource;

/**
 * Description of Source
 *
 * @author test
 */
use DateTime;

interface Source {

    public function insert(array $currency): bool;

    public function getOneByDate(DateTime $date): array;

    public function getAll(): array;
}
