/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  test
 * Created: 14 дек. 2019 г.
 */

create database if not exists currencies;
use currencies;
create table if not exists currencies (
    uid int not null auto_increment primary key,
    `code` varchar(3) not null,
    cost double(5,2) not null,
    date_at timestamp not null
) engine InnoDB;