<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Company\DataSource\{
    CurrencyAPI
};
use Company\Repository\CurrencyRepository;
use Company\Printer\ListPrinter;

$currencyApi = new CurrencyAPI();

$currencyApiRepository = new CurrencyRepository($currencyApi);

$currencies = $currencyApiRepository->list();
$printer = new ListPrinter();
$printer->show($currencies);
