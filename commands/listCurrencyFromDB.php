<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Company\DataSource\CurrencyDB;
use Company\Repository\CurrencyRepository;
use Company\Printer\ListPrinter;

$dsn = 'mysql:host=localhost;port=3306;dbname=currencies';
$user = 'root';
$password = '';
$dbConn = new PDO($dsn, $user, $password);

$currencyDb = new CurrencyDB($dbConn);
$currencyDbRepository = new CurrencyRepository($currencyDb);
$currencies = $currencyDbRepository->list();

$printer = new ListPrinter();
echo $printer->show($currencies);
