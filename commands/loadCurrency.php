<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Company\DataSource\{
    CurrencyAPI,
    CurrencyDB
};
use \Company\Repository\CurrencyRepository;

$currencyApi = new CurrencyAPI();

$dsn = 'mysql:host=localhost;port=3306;dbname=currencies';
$user = 'root';
$password = '';
$dbConn = new PDO($dsn, $user, $password);

$currencyDb = new CurrencyDB($dbConn);

$currencyApiRepository = new CurrencyRepository($currencyApi);
$currencyDbRepository = new CurrencyRepository($currencyDb);

$currency = $currencyApiRepository->get();
$currencyDbRepository->save($currency);