<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Company\DataSource\{
    CurrencyDB
};
use Company\Model\USD;
use Company\Repository\CurrencyRepository;

$dsn = 'mysql:host=localhost;port=3306;dbname=currencies';
$user = 'root';
$password = '';
$dbConn = new PDO($dsn, $user, $password);

$currencyDB = new CurrencyDB($dbConn);
$currrencyDBRepository = new CurrencyRepository($currencyDB);
$currency = new USD();
$currency->setCost(65.5);
$currency->setDateAt(DateTime::createFromFormat('Y-m-d', '2019-12-17'));

$isSaved = $currrencyDBRepository->save($currency);
echo $isSaved ? 'Saved' : 'Error';
