<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use PHPUnit\Framework\TestCase;
use Company\Printer\ListPrinter;

/**
 * Description of ListPrinterTest
 *
 * @author test
 */
class ListPrinterTest extends TestCase {

    public function testShow() {
        $printer = new ListPrinter();
        $models = ['asd'];
        $this->assertEquals(
                'asd',
                $printer->show($models)
        );

        $this->assertNotEmpty($printer->show($models));
        $this->assertContains('sd', $printer->show($models));
    }

}
