<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use PHPUnit\Framework\TestCase;
use Company\Repository\CurrencyRepository;
use Company\Model\Currency;
use Company\DataSource\Source;
use Company\Model\Rur;

/**
 * Description of CurrencyRepositoryTet
 *
 * @author test
 */
class CurrencyRepositoryTest extends TestCase {

    protected $source;

    public function tearDown(): void {
        $this->source = null;
    }

    public function setUp(): void {

        $this->source = new class implements Source {

            protected $inMemoryStore = [];

            public function insert(array $currency): bool {

                $this->inMemoryStore[] = $currency;
                return true;
            }

            public function getOneByDate(DateTime $date): array {

                foreach ($this->inMemoryStore as $rawData) {
                    if ($rawData['date_at'] === $date->format('Y-m-d H:i:s')) {
                        return $rawData;
                    }
                }

                return [];
            }

            public function getAll(): array {

                return $this->inMemoryStore;
            }
        };
    }

    public function testList() {
        $currencies = [
            [
                'code' => 'USD',
                'cost' => 65,
                'date_at' => '2019-01-01 00:00:00',
            ],
            [
                'code' => 'RUR',
                'cost' => 72,
                'date_at' => '2019-06-01 12:00:00',
            ]
        ];

        foreach ($currencies as $currency) {
            $this->source->insert($currency);
        }

        $repo = new CurrencyRepository($this->source);

        $repoCurrencies = $repo->list();
        $this->assertCount(2, $repoCurrencies);
        foreach ($repoCurrencies as $index => $repoCurrency) {
            /** @var Currency $repoCurrency */
            $this->assertInstanceOf(Currency::class, $repoCurrency);
            $this->assertEquals($currencies[$index]['code'], $repoCurrency->getCode());
            $this->assertEquals($currencies[$index]['cost'], $repoCurrency->getCost());
            $this->assertEquals($currencies[$index]['date_at'], $repoCurrency->getDateAt()->format('Y-m-d H:i:s'));
        }
    }

    public function testGet() {
        $currencies = [
            [
                'code' => 'USD',
                'cost' => 65,
                'date_at' => '2019-01-01 00:00:00',
            ],
            [
                'code' => 'RUR',
                'cost' => 72,
                'date_at' => '2019-06-01 12:00:00',
            ]
        ];

        foreach ($currencies as $currency) {
            $this->source->insert($currency);
        }

        $repo = new CurrencyRepository($this->source);
        $date1 = DateTime::createFromFormat('Y-m-d H:i:s', '2019-06-01 12:00:00');
        $currency1 = $repo->get($date1);

        $this->assertInstanceOf(Currency::class, $currency1);
        $this->assertEquals('RUR', $currency1->getCode());
        $this->assertEquals(72, $currency1->getCost());

        $date2 = DateTime::createFromFormat('Y-m-d H:i:s', '2019-01-01 00:00:00');
        $currency2 = $repo->get($date2);

        $this->assertInstanceOf(Currency::class, $currency2);
        $this->assertEquals('USD', $currency2->getCode());
        $this->assertEquals(65, $currency2->getCost());

        $date3 = DateTime::createFromFormat('Y-m-d H:i:s', '2019-12-01 00:00:00');
        $currency3 = $repo->get($date3);

        $this->assertNull($currency3);
    }

    public function testSave() {
        
        $repo = new CurrencyRepository($this->source);

        $this->assertEmpty($this->source->getAll());

        $rur = new Rur();
        $rur->setCost(10);
        $rur->setDateAt(DateTime::createFromFormat('Y-m-d H:i:s', '2018-07-07 23:23:00'));

        $repo->save($rur);

        $this->assertNotEmpty($this->source);
        $rawCurrency = array_shift($this->source->getAll());

        $this->assertEquals($rur->getCode(), $rawCurrency['code']);
        $this->assertEquals($rur->getCost(), $rawCurrency['cost']);
        $this->assertEquals($rur->getDateAt()->format('Y-m-d'), $rawCurrency['date_at']);
    }

}
