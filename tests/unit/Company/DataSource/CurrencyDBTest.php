<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use PHPUnit\Framework\TestCase;
use Company\DataSource\CurrencyDB;

/**
 * Description of ListPrinterTest
 *
 * @author test
 */
class CurrencyDBTest extends TestCase {

    protected $pdoMock;

    public function setUp(): void {

        $statement = $this->getMockBuilder(PDO::class)
                ->disableOriginalConstructor()
                ->getMock();

        $statement->method('execute')->willReturn(true);
        $statement->method('fetch')->willReturn([
            'asd' => 2
        ]);
        $statement->method('fetchAll')->willReturn([
            [
                'asd' => 2
            ]
        ]);

        $this->pdoMock = $this->getMockBuilder(PDO::class)
                ->disableOriginalConstructor()
                ->getMock();

        $this->pdoMock->method('prepare')->willReturn($statement);
    }

    public function testInsert() {

        $currencyDB = new CurrencyDB($this->pdoMock);
        $isInserted = $currencyDB->insert([]);
        $this->assartTrue($isInserted);
    }

    public function testGetOneByDate() {

        $currencyDB = new CurrencyDB($this->pdoMock);
        $data = $currencyDB->getOneByDate(new DateTime());
        $this->assartEquals(['asd' => 2], $data);
    }

    public function testGetAll() {

        $currencyDB = new CurrencyDB($this->pdoMock);
        $data = $currencyDB->getAll();
        $this->assartEquals([['asd' => 2]], $data);
    }

}
